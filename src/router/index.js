/* eslint-disable */
import en from "../i18n/lang/en";
import Vue from "vue";
import Router from "vue-router";
/*  */
Vue.use(Router);

//引入路由组件
import login from "@/pages/layout/login.vue";
import layout from "@/pages/layout/layout.vue";

import NotFound from "@/pages/page404";
import systemUserList from "../pages/systemUser/systemUserList.vue";
import roles from "@/pages/roles/roles.vue";
import menus from "../pages/menus/menus.vue";
import main from "../pages/index/main.vue";
import productTicket from "@/pages/product/ticket/ticket.vue";
import productHost from "@/pages/product/host/host.vue";
import productSupplier from "@/pages/supplier/supplier.vue";
import productLine from "@/pages/product/line/line.vue";
import productHorse from "@/pages/product/horse/horse.vue";
import productSpecialty from "@/pages/product/specialty/specialty.vue";
import supplierOrder from "@/pages/order/supplierOrder.vue";
import ticketshop from "@/pages/shop/ticketshop.vue";
import hotelShop from "@/pages/shop/hotelShop.vue";
import mcsSystemUser from "@/pages/systemUser/systemUserList.vue";

let routeName = en.routeName;
//定义路由嵌套规则
let routes = [
  // { path: '*', component: NotFoundComponent },
  {
    path: "/",
    redirect: "/index",
    hidden: true,
    children: []
  },
  {
    path: "/login",
    component: login,
    name: "",
    hidden: true,
    children: []
  },

  {
    path: "/404",
    component: NotFound,
    name: "404",
    hidden: true,
    children: []
  },

  {
    path: "/index",
    iconCls: "fa fa-dashboard", // 图标样式class
    name: routeName.home,
    component: layout,
    alone: true,
    children: [
      {
        path: "/index",
        iconCls: "fa fa-dashboard", // 图标样式class
        name: "index",
        component: main
      },
      {
        path: "/product/ticket",
        name: "productTicket",
        component: productTicket
      },
      {
        path: "/product/host",
        name: "productHost",
        component: productHost
      },
      {
        path: "/product/line",
        name: "productLine",
        component: productLine
      },
      {
        path: "/product/horse",
        name: "productHorse",
        component: productHorse
      },
      {
        path: "/product/specialty",
        name: "productSpecialty",
        component: productSpecialty
      },
      {
        path: "/merchants/audit",
        name: "productSupplier",
        component: productSupplier
      },
      {
        path: "/supplier/orders",
        name: "supplierOrder",
        component: supplierOrder
      },
      {
        path: "/merchants/ticketshop",
        name: "ticketshop",
        component: ticketshop
      },
      {
        path: "/merchants/hotelShop",
        name: "hotelShop",
        component: hotelShop
      },
      {
        path: "/user/mcsSystemUser",
        name: "mcsSystemUser",
        component: mcsSystemUser
      },
      {
        path: "/user/roles",
        name: "roles",
        component: roles
      }
    ]
  }
];
export default new Router({
  // mode: 'history',
  mode: "hash",
  routes
});
