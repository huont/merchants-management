import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import i18n from "./i18n/i18n"
import VueParticles from 'particles.js'
//引入路由配置文件
import router from './router/index.js'
import axios from 'axios'  //引入 axios库
import store from "./vuex"
import globalPlugin from "./utils/global"

import permission from "./directive/permission/button"
// 请求进度条
// import NProgress from "nprogress"

import "nprogress/nprogress.css"
import "element-ui/lib/theme-chalk/index.css"
import "@/assets/iconfont/iconfont.css"
import "font-awesome/css/font-awesome.css"
// import "@/router/permission"
//属性多选 npm i el-cascader-multi --save
import elCascaderMulti from "el-cascader-multi";

Vue.use(elCascaderMulti);

Vue.config.productionTip = false

Vue.use(VueParticles)
Vue.use(ElementUI)
Vue.use(globalPlugin)
Vue.use(permission)

// Vue.use(axios)
Vue.prototype.$http = axios;
// NProgress.inc(0.2)
// NProgress.configure({ easing: "ease", speed: 500, showSpinner: false })


Vue.prototype.getToken = function() {
	console.log("-=-=-=-=-=-=-=-=-=检测校验token");
    const token = localStorage.getItem("token");
    if (token === "" || token == null) {
		console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=token值=" +token);
        this.$message({
            showClose: true,
            message: "请先登录",
            type: "error",
            duration: "3000"
        });
        this.$router.push({ path: "/login" });
    }
    return token;
}
Vue.prototype.setToken = function(token) {
    localStorage.setItem("token", token);
}


new Vue({
  router,
  axios,
  el: '#app',
  i18n,
  store,
  render: h => h(App)

})
