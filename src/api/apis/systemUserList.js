import axios from "../axios"

import systemUserListUlrs from "../urls/systemUserList"


export default{

    selectSystemUserList(data){
      return axios.get(systemUserListUlrs.selectSystemUserList,data);
    }
}
