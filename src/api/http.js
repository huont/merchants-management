import axios from "axios";
import router from "../router";
import { Message } from "element-ui";

/**
 * 提示函数
 */
const tip = msg => {
  Message({
    message: msg,
    duration: 1000
  });
};

/**
 * 跳转登录页
 * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
 */
const toLogin = () => {
  router.replace({
    path: "/login",
    query: {
      redirect: router.currentRoute.fullPath
    }
  });
};

/**
 * 请求失败后的错误统一处理
 * @param {Number} status 请求失败的状态码
 */
const errorHandle = (status, other) => {
  // 状态码判断
  switch (status) {
    case 403:
      tip("登录过期，请重新登录");
      localStorage.removeItem("token");
      setTimeout(() => {
        toLogin();
      }, 1000);
      break;
    case 404:
      tip("请求的资源不存在");
      break;
    case 500:
      tip("服务器异常");
      break;
    default:
      tip(other);
  }
};

const instance = axios.create({
  baseURL: "http://362f6i9825.51vip.biz:27751",
  // baseURL: "https://topsystemrouter.fntopke.com",
  // baseURL: 'http://192.168.1.10:8083',
  // baseURL: 'http://localhost:8083',
  timeout: 1000000,
  responseType: "json",
  withCredentials: false
});

instance.defaults.headers.common["Authorization"] = "AUTH_TOKEN";
/**
 * 添加请求拦截器，此处还有更多配置内容，慢慢研究~
 */
instance.interceptors.request.use(
  config => {
    //在发送请求之前要做些什么？ 比如验证token 权限等内容
    // console.log("请求基本信息：", config);
    const token = localStorage.getItem("token");
    token && (config.headers.token = token);
    // console.log("最新请求基本信息：", config);
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

/**
 * 添加响应拦截器
 */
instance.interceptors.response.use(
  res => (res.status === 200 ? Promise.resolve(res) : Promise.reject(res)),
  error => {
    // return Promise.reject(error);
    const { response } = error;
    if (response) {
      // 请求已发出，但是不在2xx的范围
      errorHandle(response.status, response.data.message);
      return Promise.reject(response);
    } else {
      // 处理断网的情况
      if (!window.navigator.onLine) {
        tip("网络已断开");
      } else {
        return Promise.reject(error);
      }
    }
  }
);

export default instance;
