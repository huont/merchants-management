import http from "./http.js";

/**
 * 全局接口模块入口
 */

//用户登录
export function UserLogin(datas) {
  return http({
    // url: "http://localhost:8083/Login/login",
    url: "Login/login",
    method: "post",
    headers: {
      "Content-Type": "application/json" //设置请求头请求格式为json
    },
    data: datas
  });
}
/**
 * 首页拉取用户权限
 */
export function selectUserRolesByToken(datas) {
  return http({
    // url: "http://localhost:8083/Login/selectUserRolesByToken?token=" + datas,
    url: "Login/selectUserRolesByToken?token=" + datas,
    method: "get",

    data: datas
  });
}

/**
 * 获取商品详情  获取价格详情
 * @param {Object} url
 * @param {Object} id
 */
export function getProductById(url, productId) {
  return http({
    url: url + "?productId=" + productId,
    method: "get",
    data: productId,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}

/**
 * get page
 */
export function get(url, page, pageSize) {
  return http({
    url: url,
    method: "get",
    data: page,
    pageSize,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}
/**
 * 提交申请 修改状态
 * @param {Object} url
 * @param {Object} id
 * @param {Object} auditStatus
 */
export function getUpdataAuditStatus(url, id, auditStatus) {
  return http({
    url:
      url +
      "?systemId=" +
      localStorage.getItem("systemId") +
      "&id=" +
      id +
      "&auditStatus=" +
      auditStatus,
    method: "get",
    data: id,
    auditStatus,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}
/**
 * 获取参数 属性
 * @param {Object} url
 * @param {Object} id
 * @param {Object} propertyType
 */
export function getProductAttParam(url, id, propertyType) {
  return http({
    url: url + "?id=" + id + "&propertyType=" + propertyType,
    method: "get",
    data: id,
    propertyType,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}

/**
 * 删除图片
 * @param {Object} url
 * @param {Object} id
 * @param {Object} propertyType
 */
export function delImg(url, id) {
  return http({
    url: url + "?id=" + id,
    method: "get",
    data: id,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}
/**
 * 获取数据 用户id请求
 * @param {Object} url
 */
export function getByToken(url, datas) {
  return http({
    url: url + "?systemId=" + localStorage.getItem("systemId"),
    method: "get",
    data: datas,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}

/**
 * 获取数据 不传参
 * @param {Object} url
 */
export function getBody(url, datas) {
  return http({
    url: url,
    method: "get",
    data: datas
  });
}
/**
 * post
 */
export function post(url, datas) {
  return http({
    url: url,
    method: "post",
    headers: {
      "Content-Type": "application/json", //设置请求头请求格式为json
      token: localStorage.getItem("token") //设置请求头请求格式为json
    },
    data: datas
  });
}
/**
 * 查询供应商信息
 * @param {Object} url
 */
export function selectSuppliersById(url) {
  let id = localStorage.getItem("systemId");
  return http({
    url: url + "?id=" + id,
    method: "get",
    data: ""
  });
}
/**
 * 修改商户信息
 * @param {Object} url
 * @param {Object} datas
 */
export function updataSupplierBySystemId(url, datas) {
  return http({
    url: url,
    method: "post",
    headers: {
      "Content-Type": "application/json" //设置请求头请求格式为json
    },
    data: datas
  });
}

/**
 *
 * @param {Object} url
 * @param {Object} id
 * @param {Object} useStatus
 */
export function getUpdataOrderStatus(url, id, useStatus) {
  return http({
    url: url + "?id=" + id + "&useStatus=" + useStatus,
    method: "get",
    data: id,
    useStatus,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}

/**
 * 根据类型查询shop信息
 * @param {Object} url
 * @param {Object} shopType
 */
export function queryShopDetailBySupplier(url, shopType) {
  return http({
    url:
      url +
      "?systemId=" +
      localStorage.getItem("systemId") +
      "&shopType=" +
      shopType,
    method: "get",
    data: "",
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}
/**
 * 上传shop图片
 * @param {Object} url
 * @param {Object} id
 */
export function delShopImg(url, id) {
  return http({
    url: url + "?id=" + id,
    method: "get",
    data: id,
    headers: {
      token: localStorage.getItem("token") //设置请求头请求格式为json
    }
  });
}

/**
 * 查询用户列表
 * @param {Object} page
 * @param {Object} pageSize
 */
export function selectSystemListAll(page, pageSize, name) {
  return http({
    // url: "https://traveldistribution.fntopke.com/Login/selectUserRolesByToken?token=" + datas,
    url:
      "/supplier/systemUser/selectSupplierSystemList?page=" +
      page +
      "&pageSize=" +
      pageSize +
      "&systemId=" +
      localStorage.getItem("systemId") +
      "&name=" +
      name,
    method: "get",
    data: page,
    pageSize
  });
}

/**
 * 查询系统角色
 */
export function selectAllRoles() {
  return http({
    url: "/supplier/systemUser/selectAllRolesByLevel",
    method: "get",
    data: ""
  });
}

/**
 * 系统管理新增用户
 */
export function insertChildSystemUser(datas) {
  return http({
    // url: "https://traveldistribution.fntopke.com/Login/selectUserRolesByToken?token=" + datas,
    url: "/supplier/systemUser/insertChildSystemUser",
    method: "post",
    headers: {
      "Content-Type": "application/json" //设置请求头请求格式为json
    },
    data: datas
  });
}
/**
 * 角色列表
 * @param {Object} page
 * @param {Object} pageSize
 */
export function selectRolesByLevelPages(page, pageSize) {
  return http({
    url:
      "/supplier/systemUser/selectRolesByLevelPages?page=" +
      page +
      "&pageSize=" +
      pageSize,
    method: "get",

    data: ""
  });
}
/**
 * 修改系统管理用户状态值
 */
export function updateSystemStatus(id, status) {
  return http({
    url:
      "/supplier/systemUser/updateChildUserStatus?id=" +
      id +
      "&status=" +
      status,
    method: "get",
    data: id,
    status
  });
}
/**
 * 获取权限菜单
 */
export function selectAllMenus() {
  return http({
    url: "/supplier/systemUser/selectAllMenusBySystemId",
    method: "get",

    data: ""
  });
}
/**
 * 获取已授权数据
 * @param {Object} datas
 */
export function selectRolesMenusIdAll(datas) {
  return http({
    url:
      "/supplier/systemUser/selectChildRolesMenusIdAll?rolesId=" +
      datas +
      "&systemId=" +
      localStorage.getItem("systemId"),
    method: "get",
    data: ""
  });
}
/**
 * 授权
 * @param {Object} datas
 */
export function updateRolseAuthorization(datas) {
  return http({
    url: "/supplier/systemUser/updateRolseAuthorization",
    method: "post",
    headers: {
      "Content-Type": "application/json" //设置请求头请求格式为json
    },
    data: datas
  });
}

/**
 * 酒店商品开始
 */

// 客房设施详情
export function standardsHotelSpecificationMapperIsSelect(data) {
  return http({
    url: "/hotel/standardsHotelSpecificationMapperIsSelect",
    method: "post",
    data: data
  });
}

// 客房设施维护
export function queryHotelEquipmentMaintain(data) {
  return http({
    url: "/hotel/queryHotelEquipmentMaintain",
    method: "post",
    data: data
  });
}

// 客房设施维护-保存
export function updateStandardsHotelSpecification(data) {
  return http({
    url: "/hotel/updateStandardsHotelSpecification",
    method: "post",
    data: data
  });
}

// 房型信息详情
export function selectHotelProductDetailByProductIdIsSelect(params) {
  return http({
    url: "/hotel/selectHotelProductDetailByProductIdIsSelect",
    method: "get",
    params
  });
}

// 房型信息详情——修改
export function updateHotelProductEquipmentMaintainByProductId(data) {
  return http({
    url: "/hotel/updateHotelProductEquipmentMaintainByProductId",
    method: "post",
    data
  });
}

// 删除房型图片
export function delHotelImg(params) {
  return http({
    url: "hotel/delHotelImg",
    method: "get",
    params
  });
}

/**
 * 酒店商品结束
 */